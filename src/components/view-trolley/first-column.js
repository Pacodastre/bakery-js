import React from "react";
import { Grid, Icon, Button, Image } from "semantic-ui-react";

export default ({ item, deleteItem }) => (
  <Grid>
    <Grid.Row>
      <Grid.Column>
        <Image src={item.image} size="tiny" />
      </Grid.Column>
    </Grid.Row>
    <Grid.Row>
      <Grid.Column>
        <Button negative size="mini" onClick={() => deleteItem(item)}>
          Delete <Icon name="close" />
        </Button>
      </Grid.Column>
    </Grid.Row>
  </Grid>
);
