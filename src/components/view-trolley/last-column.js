import React from "react";
import { Grid } from "semantic-ui-react";

const LastColumn = ({ trolleyItem }) => (
  <Grid columns={1}>
    <Grid.Column>&euro;{trolleyItem.calculateTotal("string")}</Grid.Column>
  </Grid>
);

export default LastColumn;
