import React, { useState } from "react";
import { connect } from "react-redux";
import { Button, Form, Header, Message, Segment } from "semantic-ui-react";
import { useHistory } from "react-router-dom";

import StepBar from "components/step-bar";

import Payment from "domain-objects/payment";
import { placeOrder, clearOrder } from "redux/actions/order";
import { setPayment } from "redux/actions/payment";

const PaymentComponent = ({
  placeOrder,
  setPayment,
  clearOrder,
  trolley,
  payment,
}) => {
  const history = useHistory();
  const [statePayment, setStatePayment] = useState(
    new Payment({ type: "cash" })
  );
  return (
    <div>
      <StepBar step="payment" />
      <Form
        onSubmit={async (event) => {
          event.preventDefault();
          try {
            await setPayment(statePayment);
            await placeOrder();
            history.push("/order-placed");
          } catch (error) {
            alert(
              'An error has happened. Please contact us through Instagram or leave a message on the "Contact" tab of this website'
            );
          }
        }}
      >
        <Header>Amount due</Header>
        <span>&euro;{trolley.calculateTotal("string")}</span>
        <Segment>
          <Form.Field>
            <Form.Radio
              label="Cash"
              name="type"
              value="cash"
              checked={statePayment.type === "cash"}
              onChange={(event, { name, value }) =>
                setStatePayment(payment.update(name, value))
              }
            />
          </Form.Field>
          <Message
            content="Due to Covid, we would ask if you could have the exact amount ready to
            limit social interactions with the delivery person as much as
            possible."
            info
          ></Message>
        </Segment>
        <Segment>
          <Form.Field>
            <Form.Radio
              checked={statePayment.type === "contactless"}
              value="contactless"
              label="Card - Contactless"
              onChange={(event, { name, value }) =>
                setStatePayment(payment.update(name, value))
              }
              name="type"
            />
          </Form.Field>
          <Message
            content="When delivering the order, I will bring a contactless terminal so that you can pay by card"
            info
          ></Message>
        </Segment>
        <Segment>
          <Form.Field disabled>
            <Form.Radio label="Card" />
            Coming soon
          </Form.Field>
          <Message>
            This feature is not available right now. The website is very young
            and still being built. We apologize for the inconvenience and
            appreciate your cooperation.
          </Message>
          .
        </Segment>
        <Button primary type="submit">
          Place order
        </Button>
      </Form>
    </div>
  );
};

const mapStateToProps = ({ trolley, payment }) => ({ trolley, payment });

const mapDispatchToProps = { placeOrder, setPayment, clearOrder };

export default connect(mapStateToProps, mapDispatchToProps)(PaymentComponent);
