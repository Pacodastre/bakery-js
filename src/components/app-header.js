import React from "react";
import { connect } from "react-redux";
import { Header, Image } from "semantic-ui-react";
import { Link } from "react-router-dom";

const AppHeader = ({ settingManager }) => (
  <Link to="/">
    <Image
      src={settingManager.getByName("app_logo").value}
      size="small"
      style={{
        display: "block",
        marginLeft: "auto",
        marginRight: "auto",
      }}
    />
    <Header as="h2" icon textAlign="center" style={{ marginTop: "0.5em" }}>
      <Header.Content>
        {settingManager.getByName("bakery_name").value}
      </Header.Content>
      <Header sub>Artisan Bread and Pastries</Header>
      {process.env.REACT_APP_ENVIRONMENT !== "production" && (
        <Header>{process.env.REACT_APP_ENVIRONMENT}</Header>
      )}
    </Header>
  </Link>
);

const mapStateToProps = ({ settingManager }) => ({ settingManager });

export default connect(mapStateToProps)(AppHeader);
