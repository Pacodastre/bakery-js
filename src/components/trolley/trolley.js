import React from "react";
import { connect } from "react-redux";
import { Table } from "semantic-ui-react";

const Trolley = ({ trolley, productManager, specialOfferManager }) => (
  <div>
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Product</Table.HeaderCell>
          <Table.HeaderCell textAlign="right">Quantity</Table.HeaderCell>
          <Table.HeaderCell textAlign="right">Unit Price</Table.HeaderCell>
          <Table.HeaderCell textAlign="right">Price</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {trolley.items.map((trolleyItem) => {
          const product = productManager.getProduct(trolleyItem.product_id);
          const specialOffer = specialOfferManager.getSpecialOffer(
            trolleyItem.special_offer_id
          );
          return (
            <Table.Row>
              <Table.Cell>
                {trolleyItem.isProduct() ? (
                  <strong>{product.name}</strong>
                ) : (
                  <>
                    <strong>{specialOffer.name}</strong>
                    <br />
                    <span style={{ fontSize: "0.85em" }}>
                      Content
                      <ul>
                        {specialOffer
                          .getProducts(productManager)
                          .groupByProduct()
                          .map(({ product, quantity }) => (
                            <li>
                              {product.name} <strong>x{quantity}</strong>
                            </li>
                          ))}
                      </ul>
                    </span>
                  </>
                )}
              </Table.Cell>
              <Table.Cell textAlign="right">{trolleyItem.quantity}</Table.Cell>
              <Table.Cell textAlign="right">
                &euro;{product.render("price")}
              </Table.Cell>
              <Table.Cell textAlign="right">
                &euro;{trolleyItem.calculateTotal()}
              </Table.Cell>
            </Table.Row>
          );
        })}
        {trolley.items.length > 0 && (
          <Table.Row>
            <Table.Cell>Delivery</Table.Cell>
            <Table.Cell></Table.Cell>
            <Table.Cell></Table.Cell>
            <Table.Cell textAlign="right">
              &euro;{trolley.delivery_price}
            </Table.Cell>
          </Table.Row>
        )}
        <Table.Row>
          <Table.Cell>Total</Table.Cell>
          <Table.Cell></Table.Cell>
          <Table.Cell></Table.Cell>
          <Table.Cell textAlign="right">
            &euro;{trolley.calculateTotal()}
          </Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  </div>
);

const mapStateToProps = ({ trolley, productManager, specialOfferManager }) => ({
  trolley,
  productManager,
  specialOfferManager,
});

export default connect(mapStateToProps)(Trolley);
