import React from "react";
import { connect } from "react-redux";
import { Grid, Segment } from "semantic-ui-react";

import ProductCard from "components/product-card";

const ProductList = ({ trolley, message, productManager }) => {
  return (
    <Segment basic>
      <Grid stackable columns={3}>
        {productManager.items.map((product) => (
          <Grid.Column key={product.name}>
            <ProductCard product={product} />
          </Grid.Column>
        ))}
      </Grid>
    </Segment>
  );
};

const mapStateToProps = ({ trolley, message, productManager }) => ({
  trolley,
  message,
  productManager,
});

export default connect(mapStateToProps)(ProductList);
