import React from "react";
import { Table } from "semantic-ui-react";

import ReactMarkdown from "react-markdown";

const Markdown = ({ source }) => (
  <ReactMarkdown
    source={source}
    renderers={{
      table: ({ children }) => <Table>{children}</Table>,
    }}
  />
);

export default Markdown;
