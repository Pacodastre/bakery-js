import React from "react";
import { connect } from "react-redux";
import { Icon, Menu } from "semantic-ui-react";

import { setIsDelivery } from "redux/actions/order";
import Order from "domain-objects/order";

const IsDeliveryForm = ({ order, setIsDelivery }) => {
  return (
    <Menu compact id="isDelivery">
      <Menu.Item
        active={order.is_delivery}
        onClick={(event) =>
          setIsDelivery(new Order({ ...order, is_delivery: true }))
        }
      >
        <Icon name="bicycle" />
        Delivery
      </Menu.Item>
      <Menu.Item
        active={!order.is_delivery}
        onClick={(event) =>
          setIsDelivery(new Order({ ...order, is_delivery: false }))
        }
      >
        <Icon name="shopping bag" />
        Collection
      </Menu.Item>
    </Menu>
  );
};

const mapStateToProps = ({ order }) => ({ order });

const mapDispatchToProps = { setIsDelivery };

export default connect(mapStateToProps, mapDispatchToProps)(IsDeliveryForm);
