import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { Button, Checkbox, Form, Segment } from "semantic-ui-react";
import { withFormik } from "formik";

import StepBar from "components/step-bar";
import Customer from "domain-objects/customer";

import { setCustomer } from "redux/actions/customer";
import { setMessage } from "redux/actions/delivery";

const CustomFormField = (props) => {
  const {
    name,
    type,
    label,
    value,
    onChange,
    onBlur,
    placeholder,
    touched,
    error,
    renderError,
  } = props;
  return (
    <Form.Field>
      <label>{label}</label>
      <input
        style={{
          borderColor: touched
            ? error
              ? "#db2828"
              : "#21ba45"
            : "rgba(34,36,38,0.15)",
        }}
        type={type}
        onChange={onChange}
        onBlur={onBlur}
        name={name}
        value={value}
        placeholder={placeholder}
      />
      {renderError(name)}
    </Form.Field>
  );
};

const EditCustomer = ({
  values,
  touched,
  errors,
  handleChange,
  setFieldTouched,
  setFieldValue,
  handleBlur,
  handleSubmit,
  isSubmitting,
  order,
}) => {
  const renderError = (field) => {
    if (errors[field] && touched[field]) {
      return (
        <div id="feedback" style={{ color: "#db2828" }}>
          {errors[field]}
        </div>
      );
    }
  };

  const [rememberMe, setRememberMe] = useState(false);

  function saveRememberMe(rememberMe) {
    localStorage.setItem(
      "rememberMe",
      // We do not want to save the message in localStorage
      JSON.stringify(
        new Customer(rememberMe).update("message", "").renderJSON()
      )
    );
  }

  function removeRememberMe() {
    localStorage.removeItem("rememberMe");
  }

  useEffect(() => {
    // LS for local storage
    const LSrememberMe = JSON.parse(localStorage.getItem("rememberMe"));
    setRememberMe(!!LSrememberMe);
  }, [setRememberMe]);

  return (
    <Segment basic>
      <StepBar step="delivery" />
      <Form
        onSubmit={(event) => {
          if (rememberMe === true) {
            saveRememberMe(values);
          } else {
            removeRememberMe();
          }
          handleSubmit(event);
        }}
      >
        <CustomFormField
          label="First Name"
          name="first_name"
          type="text"
          touched={touched.first_name}
          error={errors.first_name}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="First Name"
          value={values.first_name}
          renderError={renderError}
        />
        <CustomFormField
          label="Last Name"
          name="last_name"
          type="text"
          touched={touched.last_name}
          error={errors.last_name}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="Last Name"
          value={values.last_name}
          renderError={renderError}
        />
        <CustomFormField
          label="Phone"
          name="phone"
          type="text"
          touched={touched.phone}
          error={errors.phone}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="Phone"
          value={values.phone}
          renderError={renderError}
        />
        <CustomFormField
          label="Email address"
          name="email"
          type="email"
          touched={touched.email}
          error={errors.email}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="Email address"
          value={values.email}
          renderError={renderError}
        />
        {order.is_delivery && (
          <>
            <CustomFormField
              label="Address Line 1"
              name="address1"
              type="text"
              touched={touched.address1}
              error={errors.address1}
              onChange={handleChange}
              onBlur={handleBlur}
              placeholder="Address 1"
              value={values.address1}
              renderError={renderError}
            />
            <CustomFormField
              label="Address Line 2"
              name="address2"
              type="text"
              touched={touched.address2}
              error={errors.address2}
              onChange={handleChange}
              onBlur={handleBlur}
              placeholder="Address 2"
              value={values.address2}
              renderError={renderError}
            />
            <Form.Field>
              <label>City</label>
              <input disabled value={values.city} />
            </Form.Field>
            <Form.Field>
              <label>County</label>
              <input disabled value={values.county} />
            </Form.Field>
          </>
        )}
        <Form.Field>
          <label>
            Leave a message (food allergies, directions for delivery...)
          </label>
          <textarea
            style={{
              borderColor: touched.message
                ? errors.message
                  ? "#db2828"
                  : "#21ba45"
                : "rgba(34,36,38,0.15)",
            }}
            value={values.message}
            name="message"
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </Form.Field>
        <Form.Field>
          <Checkbox
            label="Remember me on this device"
            onChange={(event, { checked }) => setRememberMe(checked)}
            checked={rememberMe}
          />
        </Form.Field>
        <Button primary disabled={isSubmitting} type="submit">
          Continue to Payment
        </Button>
      </Form>
    </Segment>
  );
};

const EditCustomerForm = withFormik({
  initialValues: localStorage.rememberMe
    ? JSON.parse(localStorage.rememberMe)
    : new Customer(),
  enableReinitialize: true,
  mapPropsToValues: ({ order }) => {
    const rememberMe = localStorage.getItem("rememberMe");
    return !!rememberMe ? new Customer(JSON.parse(rememberMe)) : new Customer();
  },
  validate: (values, { order }) => {
    const errors = {};
    if (!values.first_name) {
      errors.first_name = "Required";
    }
    if (!values.last_name) {
      errors.last_name = "Required";
    }
    if (!values.phone) {
      errors.phone = "Required";
    }
    if (!values.email) {
      errors.email = "Required";
    }
    if (values.email && values.email.indexOf("@") === -1) {
      errors.email = "Please enter a valid email address";
    }
    if (order.is_delivery && !values.address1) {
      errors.address1 = "Required";
    }
    if (order.is_delivery && !values.address2) {
      errors.address2 = "Required";
    }
    return errors;
  },

  handleSubmit: async (values, { props, setSubmitting }) => {
    await props.setCustomer(
      new Customer({
        ...props.customer,
        ...values,
      })
    );
    await props.setMessage(values.message);
    setSubmitting(false);
    props.history.push("/payment");
  },
  displayName: "AddressForm",
})(EditCustomer);

const mapStateToProps = ({ customer, order }) => ({ customer, order });

const mapDispatchToProps = (dispatch) => {
  return {
    setCustomer: (customer) => dispatch(setCustomer(customer)),
    setMessage: (delivery, message) => dispatch(setMessage(delivery, message)),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(EditCustomerForm)
);
