import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Button, Header } from "semantic-ui-react";
import { Link } from "react-router-dom";

import IsDeliveryForm from "components/orders/is-delivery-form";
import ProductList from "components/product-list";
import SpecialOfferList from "components/special-offer-list";
import TrolleyButton from "components/trolley/trolley-button";
import { fetchProducts, clearProducts } from "redux/actions/product-manager";
import { fetchAllergens, clearAllergens } from "redux/actions/allergen-manager";
import {
  fetchSpecialOffers,
  clearSpecialOffers,
} from "redux/actions/special-offer-manager";

const HomePage = ({
  fetchProducts,
  clearProducts,
  fetchAllergens,
  clearAllergens,
  specialOfferManager,
  fetchSpecialOffers,
  clearSpecialOffers,
}) => {
  useEffect(() => {
    async function fetch() {
      await fetchAllergens();
      await fetchProducts();
      await fetchSpecialOffers();
    }
    fetch();
    return () => {
      clearProducts();
      clearAllergens();
      clearSpecialOffers();
    };
  }, [
    fetchProducts,
    clearProducts,
    fetchAllergens,
    clearAllergens,
    clearSpecialOffers,
    fetchSpecialOffers,
  ]);

  return (
    <>
      <div style={{ margin: "30px auto 0", width: 200 }}>
        <IsDeliveryForm />
      </div>
      {specialOfferManager.items.length > 0 && (
        <>
          <Header as="h1">Special Offers</Header>
          <SpecialOfferList />
        </>
      )}
      <Header as="h1">Products</Header>
      <ProductList />
      <div style={{ height: 70 }} />
      <div style={{ position: "fixed", right: 20, bottom: 20 }}>
        <Button primary size="big" as={Link} to="/view-trolley">
          <TrolleyButton />
        </Button>
      </div>
    </>
  );
};

const mapStateToProps = ({ specialOfferManager }) => ({ specialOfferManager });

const mapDispatchToProps = {
  fetchProducts,
  clearProducts,
  fetchAllergens,
  clearAllergens,
  fetchSpecialOffers,
  clearSpecialOffers,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
