import React from "react";
import { Header } from "semantic-ui-react";

import EditBooking from "components/edit-booking";
import StepBar from "components/step-bar";

const EditBookingPage = () => (
  <>
    <StepBar step="booking" />
    <Header>Select delivery date and time</Header>
    <EditBooking />
  </>
);

export default EditBookingPage;
