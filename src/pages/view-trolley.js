import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Grid, Segment, Button, Message } from "semantic-ui-react";
import { Link } from "react-router-dom";

import StepBar from "components/step-bar";
import FirstColumn from "components/view-trolley/first-column";
import MiddleColumn from "components/view-trolley/middle-column";
import LastColumn from "components/view-trolley/last-column";
import Delivery from "components/view-trolley/delivery";
import Total from "components/view-trolley/total";

import { fetchProducts, clearProducts } from "redux/actions/product-manager";
import {
  deleteProduct,
  deleteSpecialOffer,
  addOneProduct,
  removeOneProduct,
  addOneSpecialOffer,
  removeOneSpecialOffer,
} from "redux/actions/trolley";
import {
  fetchSpecialOffers,
  clearSpecialOffers,
} from "redux/actions/special-offer-manager";
import IsDeliveryForm from "components/orders/is-delivery-form";

const ViewTrolleyPage = ({
  trolley,
  productManager,
  specialOfferManager,
  fetchProducts,
  clearProducts,
  fetchSpecialOffers,
  clearSpecialOffers,
  deleteProduct,
  deleteSpecialOffer,
  addOneProduct,
  addOneSpecialOffer,
  removeOneSpecialOffer,
  removeOneProduct,
  isDelivery,
  order,
  settingManager,
}) => {
  useEffect(() => {
    fetchProducts();
    fetchSpecialOffers();
    return () => {
      clearProducts();
      clearSpecialOffers();
    };
  }, [fetchProducts, clearProducts, fetchSpecialOffers, clearSpecialOffers]);

  return (
    <div>
      <StepBar step="view-trolley" />
      {isDelivery && !order.isTrolleyValid(trolley, settingManager) && (
        <Message warning>
          You need to spend €
          {settingManager.getByName("delivery_minimum_order").value} or more
          before delivery to place an order.
        </Message>
      )}
      {!isDelivery && !order.isTrolleyValid(trolley, settingManager) && (
        <Message warning>
          You need to spend €
          {settingManager.getByName("collection_minimum_order").value} or more
          to place an order.
        </Message>
      )}

      {trolley.numberItems() === 0 && (
        <Message warning>
          Your trolley is empty, add some items to continue
        </Message>
      )}
      <Grid textAlign="center">
        <IsDeliveryForm />
      </Grid>
      <Segment basic>
        {trolley.numberItems() > 0 ? (
          <>
            <Grid divided="vertically">
              {productManager.items.length > 0 &&
                trolley.items.map((trolleyItem) => {
                  const product =
                    trolleyItem.product_id &&
                    productManager.getProduct(trolleyItem.product_id);
                  const specialOffer =
                    trolleyItem.special_offer_id &&
                    specialOfferManager.getSpecialOffer(
                      trolleyItem.special_offer_id
                    );
                  if (!product && !specialOffer) {
                    return <></>;
                  }
                  return (
                    <Grid.Row
                      key={
                        trolleyItem.isProduct() ? product.key : specialOffer.id
                      }
                    >
                      <Grid.Column width={4}>
                        <FirstColumn
                          item={
                            trolleyItem.isProduct() ? product : specialOffer
                          }
                          deleteItem={
                            trolleyItem.isProduct()
                              ? deleteProduct
                              : deleteSpecialOffer
                          }
                        />
                      </Grid.Column>
                      <Grid.Column width={8}>
                        <MiddleColumn
                          item={
                            trolleyItem.isProduct() ? product : specialOffer
                          }
                          addOneItem={
                            trolleyItem.isProduct()
                              ? addOneProduct
                              : addOneSpecialOffer
                          }
                          removeOneItem={
                            trolleyItem.isProduct()
                              ? removeOneProduct
                              : removeOneSpecialOffer
                          }
                          trolleyItem={trolleyItem}
                          productManager={productManager}
                        />
                      </Grid.Column>
                      <Grid.Column
                        width={4}
                        textAlign="right"
                        verticalAlign="middle"
                      >
                        <LastColumn trolleyItem={trolleyItem} />
                      </Grid.Column>
                    </Grid.Row>
                  );
                })}
              <Grid.Row>
                <Delivery trolley={trolley} />
              </Grid.Row>
              <Grid.Row>
                <Total trolley={trolley} />
              </Grid.Row>
            </Grid>
            <Link to="/booking">
              <Button
                primary
                disabled={!order.isTrolleyValid(trolley, settingManager)}
              >
                Continue to Booking
              </Button>
            </Link>
          </>
        ) : (
          <div style={{ textAlign: "center" }}>
            <Button primary as={Link} to="/">
              Go back to product list
            </Button>
          </div>
        )}
      </Segment>
    </div>
  );
};

const mapStateToProps = ({
  trolley,
  productManager,
  specialOfferManager,
  settingManager,
  order,
}) => ({
  trolley,
  productManager,
  specialOfferManager,
  isDelivery: order.is_delivery,
  settingManager,
  order,
});
const mapDispatchToProps = {
  fetchProducts,
  clearProducts,
  fetchSpecialOffers,
  clearSpecialOffers,
  deleteProduct,
  deleteSpecialOffer,
  addOneProduct,
  addOneSpecialOffer,
  removeOneProduct,
  removeOneSpecialOffer,
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewTrolleyPage);
