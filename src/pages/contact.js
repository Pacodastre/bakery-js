import React, { useState } from "react";
import { connect } from "react-redux";
import { Button, Message, Segment, Form } from "semantic-ui-react";

import CustomerMessage from "domain-objects/customer-message";
import { sendMessage } from "redux/actions/customer-message";
import { clearFormErrorManager } from "redux/actions/form-error-manager";

const ContactPage = ({
  sendMessage,
  formErrorManager,
  clearFormErrorManager,
}) => {
  const [customerMessage, setStateMessage] = useState(new CustomerMessage());
  const [info, setInfo] = useState(null);
  const [error, setError] = useState(null);
  return (
    <Segment basic>
      <h2>Contact</h2>
      <h4>
        Do you have any questions? Do you simply want to say something to the
        baker? Drop us a message!
      </h4>
      {info && <Message info>{info}</Message>}
      {error && <Message error>{error}</Message>}
      <Form
        onSubmit={async (event) => {
          event.preventDefault();
          await sendMessage(customerMessage, setInfo, setError, () => {
            setStateMessage(new CustomerMessage());
            clearFormErrorManager();
          });
        }}
      >
        <Form.Input
          error={
            formErrorManager.hasError("name") &&
            formErrorManager.errorMessage("name")
          }
          id="name"
          name="name"
          value={customerMessage.name}
          onChange={(event) =>
            setStateMessage(
              customerMessage.update(event.target.name, event.target.value)
            )
          }
          label="Your name"
        />
        <Form.Input
          error={
            formErrorManager.hasError("email") &&
            formErrorManager.errorMessage("email")
          }
          label="Your email address"
          id="email"
          name="email"
          value={customerMessage.email}
          onChange={(event) =>
            setStateMessage(
              customerMessage.update(event.target.name, event.target.value)
            )
          }
        />
        <Form.TextArea
          error={
            formErrorManager.hasError("message") &&
            formErrorManager.errorMessage("message")
          }
          label="Your message"
          rows={10}
          id="message"
          name="message"
          value={customerMessage.message}
          onChange={(bla, ble, bli) =>
            setStateMessage(
              customerMessage.update(bla.target.name, bla.target.value)
            )
          }
        />
        <Button type="submit">Send message</Button>
      </Form>
    </Segment>
  );
};

const mapStateToProps = ({ formErrorManager }) => ({ formErrorManager });

const mapDispatchToProps = { sendMessage, clearFormErrorManager };

export default connect(mapStateToProps, mapDispatchToProps)(ContactPage);
