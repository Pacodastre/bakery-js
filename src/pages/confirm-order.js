import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Button, Form, Header, Segment } from "semantic-ui-react";
import { useParams, useHistory } from "react-router-dom";

import {
  fetchSpecialOffers,
  clearSpecialOffers,
} from "redux/actions/special-offer-manager";
import { fetchProducts, clearProducts } from "redux/actions/product-manager";
import { fetchOrder, clearOrder, confirmOrder } from "redux/actions/order";
import Trolley from "components/trolley/trolley";
import { amPm, formatDate, formatTime, dayOfWeek } from "utils/date";

const ConfirmOrderPage = ({
  fetchOrder,
  clearOrder,
  delivery,
  confirmOrder,
  order,
  fetchProducts,
  clearProducts,
  fetchSpecialOffers,
  clearSpecialOffers,
}) => {
  const { orderId, confirmationKey } = useParams();
  const history = useHistory();
  useEffect(() => {
    fetchProducts();
    fetchSpecialOffers();
    fetchOrder(orderId, confirmationKey);
    return () => {
      clearOrder();
      clearProducts();
      clearSpecialOffers();
    };
  }, [
    orderId,
    confirmationKey,
    clearOrder,
    fetchOrder,
    fetchProducts,
    clearProducts,
    fetchSpecialOffers,
    clearSpecialOffers,
  ]);
  return (
    <Segment basic>
      <Header>
        Please confirm your order
        <Header.Subheader>Order #{order.id}</Header.Subheader>
      </Header>
      <p>
        The order was placed on {formatDate(order.inserted_at)} at{" "}
        {formatTime(order.inserted_at)}.
      </p>
      <Trolley />
      <ul>
        <li>Type of payment: Cash</li>
        <li>
          Delivery date:{" "}
          {delivery.delivery_date &&
            dayOfWeek(delivery.toMoment("delivery_date"))}{" "}
          {delivery.renderField("delivery_date")} between{" "}
          {`${delivery.renderField("time_slot")}`.padStart(2, "0")}:00
          {amPm(delivery.time_slot)} and{" "}
          {`${delivery.renderField("time_slot") + 1}`.padStart(2, "0")}:00
          {amPm(delivery.time_slot + 1)}.
        </li>
      </ul>
      <Form
        onSubmit={async (event) => {
          event.preventDefault();
          try {
            await confirmOrder(orderId, confirmationKey);
            history.push("/order-confirmed");
          } catch (error) {
            alert("Error: " + error.message);
          }
        }}
      >
        <Button type="submit" primary>
          Confirm Order
        </Button>
      </Form>
    </Segment>
  );
};

const mapStateToProps = ({ delivery, order }) => ({ delivery, order });

const mapDispatchToProps = {
  fetchOrder,
  clearOrder,
  confirmOrder,
  fetchProducts,
  clearProducts,
  fetchSpecialOffers,
  clearSpecialOffers,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmOrderPage);
