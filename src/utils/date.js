import moment from "moment";

const DATE_FORMATS = {
  frontend_datetime: "DD/MM/YYYY HH:mm",
  frontend_date: "DD/MM/YYYY",
  frontend_time: "HH:mm",
  backend_datetime: "YYYY-MM-DD HH:mm:00",
  backend_date: "YYYY-MM-DD",
  backend_time: "HH:mm:00",
};

export function formatDateTime(datetime) {
  if (!datetime) {
    return "";
  }
  return moment
    .utc(datetime, moment.ISO_8601)
    .local()
    .format(DATE_FORMATS.frontend_datetime);
}

export function formatDate(date) {
  if (!date) {
    return "";
  }
  return moment
    .utc(date, moment.ISO_8601)
    .local()
    .format(DATE_FORMATS.frontend_date);
}

export function formatDateToBackend(date) {
  return moment(date).format("YYYY-MM-DD");
}

export function formatDateTimeToBackend(datetime) {
  return moment(datetime).toISOString();
}

export function formatTime(datetime) {
  return moment(datetime).format(DATE_FORMATS.frontend_time);
}

export const WEEK_DAYS = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];

export function dayOfWeek(datetime) {
  return WEEK_DAYS[datetime.weekday()];
}

export function amPm(time) {
  return time >= 12 ? "pm" : "am";
}
