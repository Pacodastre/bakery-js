export function range(size, startAt = 0) {
  return [...Array(size).keys()].map((i) => i + startAt);
}

export function getBackgroundColor() {
  switch (process.env.REACT_APP_ENVIRONMENT) {
    case "local": {
      return "#8383a9";
    }
    case "testing": {
      return "#38604a";
    }
    default: {
      return "inherit";
    }
  }
}
