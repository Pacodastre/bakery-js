import {
  PLACE_ORDER,
  CLEAR_ORDER,
  FETCH_ORDER,
  SET_IS_DELIVERY,
} from "redux/actions/action-types";

import { createOrder } from "domain-objects/order";

export function orderReducer(state = createOrder(), action) {
  switch (action.type) {
    case SET_IS_DELIVERY:
    case FETCH_ORDER:
    case PLACE_ORDER: {
      return action.payload;
    }
    case CLEAR_ORDER: {
      return createOrder();
    }
    default: {
      return state;
    }
  }
}
