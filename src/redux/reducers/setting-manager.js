import { GET_SETTINGS, CLEAR_SETTINGS } from "redux/actions/action-types";

import SettingManager from "domain-objects/setting-manager";

export function settingManagerReducer(state = new SettingManager(), action) {
  switch (action.type) {
    case GET_SETTINGS: {
      return action.payload;
    }
    case CLEAR_SETTINGS: {
      return new SettingManager();
    }
    default: {
      return state;
    }
  }
}