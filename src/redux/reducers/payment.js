import { SET_PAYMENT, CLEAR_PAYMENT } from "redux/actions/action-types";
import Payment from "domain-objects/payment";

export function paymentReducer(state = new Payment(), action) {
  switch (action.type) {
    case SET_PAYMENT: {
      return action.payload;
    }
    case CLEAR_PAYMENT: {
      return new Payment();
    }
    default: {
      return state;
    }
  }
}
