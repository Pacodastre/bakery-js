import axios from "redux/actions/axios";
import {
  GET_BOOKING_SLOTS,
  CLEAR_BOOKING_SLOTS,
} from "redux/actions/action-types";

import { createBookingSlotManager } from "domain-objects/booking-slot-manager";
import { formatDateToBackend } from "utils/date";

export function fetchBookingSlots(date = new Date()) {
  const dateString = formatDateToBackend(date);
  return (dispatch, getState) => {
    axios.get(`/booking_slots/?date=${dateString}`).then((response) =>
      dispatch({
        type: GET_BOOKING_SLOTS,
        payload: createBookingSlotManager(response.data.data),
      })
    );
  };
}

export function clearBookingSlots() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_BOOKING_SLOTS });
  };
}
