import { SET_PAYMENT, CLEAR_PAYMENT } from "redux/actions/action-types";

export function setPayment(payment) {
  return (dispatch, getState) => {
    dispatch({ type: SET_PAYMENT, payload: payment });
  };
}

export function clearPayment() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_PAYMENT });
  };
}
