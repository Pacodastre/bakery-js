import axios from "redux/actions/axios";
import { GET_PRODUCTS, CLEAR_PRODUCTS } from "redux/actions/action-types";

import ProductManager from "domain-objects/product-manager";
import Product from "domain-objects/product";

export function fetchProducts() {
  return (dispatch, getState) => {
    axios.get("/products/").then((response) =>
      dispatch({
        type: GET_PRODUCTS,
        payload: new ProductManager(
          response.data.data.map((product) => new Product(product))
        ),
      })
    );
  };
}

export function clearProducts() {
  return (dispatch, getState) => dispatch({ type: CLEAR_PRODUCTS });
}
