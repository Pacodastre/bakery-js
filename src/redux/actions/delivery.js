import {
  SET_DELIVERY,
  CLEAR_DELIVERY,
  SET_DELIVERY_MESSAGE,
} from "redux/actions/action-types";

import Delivery from "domain-objects/delivery";

export function setDelivery(delivery) {
  return async (dispatch, getState) => {
    await dispatch({ type: SET_DELIVERY, payload: delivery });
  };
}

export function clearDelivery() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_DELIVERY });
  };
}

export function setMessage(message) {
  return (dispatch, getState) => {
    dispatch({
      type: SET_DELIVERY_MESSAGE,
      payload: new Delivery({ ...getState().delivery, message }),
    });
  };
}
