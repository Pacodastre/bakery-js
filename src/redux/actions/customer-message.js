import axios from "redux/actions/axios";

import { setFormErrorManager } from "redux/actions/form-error-manager";

export function sendMessage(customerMessage, setInfo, setError, callback) {
  return (dispatch, getState) => {
    axios
      .post("/messages", { message: customerMessage.renderJSON() })
      .then(() => {
        setInfo("Message successfully sent");
        setTimeout(() => setInfo(null), 3000);
        if (callback) {
          callback();
        }
      })
      .catch((error) => {
        setError("Something went wrong. Please check the form.");
        setFormErrorManager(error.response.data.errors)(dispatch, getState);
        setTimeout(() => setError(null), 5000);
      });
  };
}
