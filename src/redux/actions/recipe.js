import axios from "redux/actions/axios";
import { GET_RECIPE, CLEAR_RECIPE } from "redux/actions/action-types";
import Recipe from "domain-objects/recipe";

export function fetchRecipe(productId) {
  return (dispatch, getState) => {
    axios
      .get(`/recipes/by-product?product_id=${productId}`)
      .then((response) =>
        dispatch({ type: GET_RECIPE, payload: new Recipe(response.data.data) })
      );
  };
}

export function clearRecipe() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_RECIPE });
  };
}
