import axios from "redux/actions/axios";
import { GET_SETTINGS, CLEAR_SETTINGS } from "redux/actions/action-types";

import { createSettingManager } from "domain-objects/setting-manager";
import { updateDeliveryPrice } from "redux/actions/trolley";

export function getSettings() {
  return async (dispatch, getState) => {
    await axios.get("/settings/").then(async (response) => {
      await dispatch({
        type: GET_SETTINGS,
        payload: createSettingManager(response.data.data),
      });
      const settingManager = getState().settingManager;
      const deliveryPrice = settingManager.getByName("delivery_price").value;
      await updateDeliveryPrice(deliveryPrice)(dispatch, getState);
    });
  };
}

export function clearSettings() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_SETTINGS });
  };
}
