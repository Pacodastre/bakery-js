export default class Setting {
  constructor({ id, name, value, type } = {}) {
    this.id = id;
    this.name = name;
    this.type = type;
    function getValue(type, value) {
      switch(type) {
        case "float": {
          return parseFloat(value);
        }
        default: {
          return value;
        }
      }
    }
    this.value = getValue(type, value);
  }
}