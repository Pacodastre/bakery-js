import TrolleyItem from "domain-objects/trolley-item";

export default class Trolley {
  constructor({ items = [], delivery_price } = {}) {
    this.items = items;
    this.delivery_price = delivery_price;
  }

  isEmpty() {
    return this.items.length === 0;
  }

  findTrolleyItemByProduct(product) {
    return this.items.find((item) => item.product_id === product.id);
  }

  findTrolleyItemBySpecialOffer(specialOffer) {
    return this.items.find((item) => item.special_offer_id === specialOffer.id);
  }

  calculateTotalWithoutDelivery(format) {
    const total = this.items.reduce((total, trolleyItem) => {
      return total + trolleyItem.calculateTotal();
    }, 0);
    const output = Math.round(total * 100) / 100;
    if (format === "string" && !Number.isInteger(output)) {
      return output.toFixed(2);
    }
    return output;
  }

  calculateTotal(format) {
    // Add delivery_price if there's more than one item in the trolley
    const total =
      this.calculateTotalWithoutDelivery() + this.calculateDeliveryPrice();
    const output = Math.round(total * 100) / 100;
    if (format === "string" && !Number.isInteger(format)) {
      return output.toFixed(2);
    }
    return output;
  }

  calculateDeliveryPrice() {
    return this.items.length > 0 ? this.delivery_price : 0;
  }

  numberItems() {
    return this.items.reduce(
      (total, trolleyItem) => trolleyItem.quantity + total,
      0
    );
  }

  _addOneItem(itemToAdd, attribute) {
    // itemToAdd can either be a product or a special_offer
    // attribute is either "product_id" or "special_offer_id"
    if (
      !this.items.find((trolleyItem) => trolleyItem[attribute] === itemToAdd.id)
    ) {
      return new Trolley({
        ...this,
        items: [
          ...this.items,
          new TrolleyItem({
            [attribute]: itemToAdd.id,
            quantity: 1,
            unit_price: itemToAdd.price,
          }),
        ],
      });
    } else {
      const item = this.items.find(
        (trolleyItem) => trolleyItem[attribute] === itemToAdd.id
      );
      const indexOfItem = this.items.indexOf(item);
      return new Trolley({
        ...this,
        items: [
          ...this.items.slice(0, indexOfItem),
          new TrolleyItem({ ...item, quantity: item.quantity + 1 }),
          ...this.items.slice(indexOfItem + 1),
        ],
      });
    }
  }

  _removeOneItem(itemToRemove, attribute) {
    const item = this.items.find(
      (trolleyItem) => trolleyItem[attribute] === itemToRemove.id
    );
    const indexOfItem = this.items.indexOf(item);
    const quantity = item.quantity - 1 >= 0 ? item.quantity - 1 : 0;
    if (quantity === 0) {
      return new Trolley({
        ...this,
        items: [
          ...this.items.slice(0, indexOfItem),
          ...this.items.slice(indexOfItem + 1),
        ],
      });
    }
    return new Trolley({
      ...this,
      items: [
        ...this.items.slice(0, indexOfItem),
        new TrolleyItem({ ...item, quantity }),
        ...this.items.slice(indexOfItem + 1),
      ],
    });
  }

  _deleteItem(itemToDelete, attribute) {
    const item = this.items.find(
      (trolleyItem) => trolleyItem[attribute] === itemToDelete.id
    );
    const indexOfItem = this.items.indexOf(item);
    return new Trolley({
      ...this,
      items: [
        ...this.items.slice(0, indexOfItem),
        ...this.items.slice(indexOfItem + 1),
      ],
    });
  }

  updateDeliveryPrice(deliveryPrice) {
    return createTrolley({ ...this, delivery_price: deliveryPrice});
  }

  addOneProduct(product) {
    return this._addOneItem(product, "product_id");
  }

  addOneSpecialOffer(specialOffer) {
    return this._addOneItem(specialOffer, "special_offer_id");
  }

  removeOneProduct(product) {
    return this._removeOneItem(product, "product_id");
  }

  removeOneSpecialOffer(specialOffer) {
    return this._removeOneItem(specialOffer, "special_offer_id");
  }

  deleteProduct(product) {
    return this._deleteItem(product, "product_id");
  }

  deleteSpecialOffer(specialOffer) {
    return this._deleteItem(specialOffer, "special_offer_id");
  }
}

export function createTrolley(trolley = new Trolley()) {
  return new Trolley({
    ...trolley,
    items: trolley.items.map((item) => new TrolleyItem(item)),
  });
}
