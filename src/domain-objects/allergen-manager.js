import Allergen from "domain-objects/allergen";

export default class AllergenManager {
  constructor(items = []) {
    this.items = items;
  }

  getAllergen(id) {
    return this.items.find((item) => item.id === id) || new Allergen();
  }
}
