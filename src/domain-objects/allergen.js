export default class Allergen {
  constructor({ id, name = "", image = "" } = {}) {
    this.id = id;
    this.name = name;
    this.image = image;
  }
}
