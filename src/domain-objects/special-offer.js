import ProductManager from "domain-objects/product-manager";

export default class SpecialOffer {
  constructor({ id, disabled, name, order, price, image, products = [] } = {}) {
    this.id = id;
    this.disabled = disabled;
    this.name = name;
    this.order = order;
    this.price = price;
    this.image = image;
    this.products = products;
  }

  getProducts(productManager) {
    return new ProductManager(
      this.products.map((productId) => productManager.getProduct(productId))
    );
  }

  render(field) {
    switch (field) {
      case "price": {
        return Number.isInteger(this[field])
          ? this[field]
          : this[field].toFixed(2);
      }
      default: {
        return this[field];
      }
    }
  }
}
