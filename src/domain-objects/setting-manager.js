import Setting from "domain-objects/setting";

export default class SettingManager {
  constructor(items = []) {
    this.items = items;
  }

  getByName(name) {
    return this.items.find((setting) => setting.name === name) || new Setting();
  }
}

export function createSettingManager(items) {
  return new SettingManager(items.map((setting) => new Setting(setting)));
}