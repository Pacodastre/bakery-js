import Product from "domain-objects/product";
import AllergenManager from "domain-objects/allergen-manager";

export default class ProductManager {
  constructor(items = []) {
    this.items = items;
  }

  groupByProduct() {
    // { product: product, quantity: 2 }
    return this.items.reduce((accumulator, product) => {
      const item = accumulator.find((item) => item.product.id === product.id);
      if (item) {
        return [
          ...accumulator.slice(0, accumulator.indexOf(item)),
          { product: product, quantity: item.quantity + 1 },
          ...accumulator.slice(accumulator.indexOf(item) + 1),
        ];
      }
      return [...accumulator, { product: product, quantity: 1 }];
    }, []);
  }

  getProduct(id) {
    return this.items.find((item) => item.id === id) || new Product();
  }

  filter(type = "all") {
    if (type === "all") return this.data;
    else return this.items.filter((product) => product.type === type);
  }

  getAllergens(allergenManager) {
    return new AllergenManager([
      ...new Set(
        this.items
          .map((product) => product.getAllergens(allergenManager).items)
          .flat()
      ),
    ]);
  }

  getMayContainAllergens(allergenManager) {
    return new AllergenManager([
      ...new Set(
        this.items
          .map(
            (product) => product.getMayContainAllergens(allergenManager).items
          )
          .flat()
      ),
    ]);
  }
}
