export default class CustomerMessage {
  constructor({ name = "", email = "", message = "" } = {}) {
    this.name = name;
    this.email = email;
    this.message = message;
  }

  update(field, value) {
    return new CustomerMessage({
      ...this,
      [field]: value,
    });
  }

  renderJSON() {
    return {
      name: this.name,
      email: this.email,
      message: this.message,
    };
  }
}
