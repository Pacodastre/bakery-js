import AllergenManager from "domain-objects/allergen-manager";

export default class Product {
  constructor({
    id,
    key = "",
    name = "",
    description = "",
    image = "",
    price = 0,
    type = "bread",
    allergens = [],
    may_contain_allergens = [],
  } = {}) {
    this.id = id;
    this.key = key;
    this.name = name;
    this.description = description;
    this.image = image;
    this.price = price;
    this.type = type;
    this.allergens = allergens;
    this.may_contain_allergens = may_contain_allergens;
  }

  getAllergens(allergenManager) {
    return new AllergenManager(
      this.allergens.map((allergenID) =>
        allergenManager.getAllergen(allergenID)
      )
    );
  }

  getMayContainAllergens(allergenManager) {
    return new AllergenManager(
      this.may_contain_allergens.map((allergenID) =>
        allergenManager.getAllergen(allergenID)
      )
    );
  }

  render(field) {
    switch (field) {
      case "price": {
        return Number.isInteger(this[field])
          ? this[field]
          : this[field].toFixed(2);
      }
      default: {
        return this[field];
      }
    }
  }
}
