export default class Payment {
  constructor({ type = "" } = {}) {
    this.type = type;
  }

  isValid() {
    return !!this.type;
  }

  update(name, value) {
    return new Payment({ ...this, [name]: value });
  }
}
