import FormError from "domain-objects/form-error";

export default class FormErrorManager {
  constructor(errors = []) {
    this.errors = errors;
  }

  hasError(field) {
    return !!this.errors.find((error) => error.field === field);
  }

  errorMessage(field) {
    const error = this.errors.find((error) => error.field === field);
    console.log(error && error.value[0]);
    return error && error.value[0];
  }
}

export function createFormErrorManager(errors = {}) {
  return new FormErrorManager(
    Object.keys(errors).map(
      (field) => new FormError({ field: field, value: errors[field] })
    )
  );
}
